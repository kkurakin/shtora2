//
//

//#include <stdlib.h>
#include <string.h>
#include "misc.h"
#include "stm32f4xx_can.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "can.h"
#include "queue.h"
#include "message.h"
#include "memory.h"

//------------------------------------------------------------------------------

#define CAN_TX_TIMEOUT 1000

static CanTxQueue *txQueue = 0;
static CanRxQueue *rxQueue = 0;
static CanTxMsg txMess;
static CanRxMsg rxMess;

static CanTxQueue *newTxQueue(uint8_t *data, uint32_t length, uint8_t sender);
static void onCanDataSent(uint8_t error, uint8_t sent);
static void onCanDataReceived(void);

//------------------------------------------------------------------------------

void initCanHW(void)
{
    CAN_InitTypeDef can;
    CAN_FilterInitTypeDef filter;
    GPIO_InitTypeDef gpio;
    NVIC_InitTypeDef nvic;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

    GPIO_PinAFConfig(CAN_GPIO, CAN_GPIO_PIN_SOURCE_RX, GPIO_AF_CAN1);
    GPIO_PinAFConfig(CAN_GPIO, CAN_GPIO_PIN_SOURCE_TX, GPIO_AF_CAN1);

    gpio.GPIO_Pin = CAN_GPIO_PIN_RX | CAN_GPIO_PIN_TX;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(CAN_GPIO, &gpio);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

    CAN_DeInit(CAN1);

    can.CAN_TTCM = DISABLE;
    can.CAN_ABOM = ENABLE;
    can.CAN_AWUM = DISABLE;
    can.CAN_NART = ENABLE;
    can.CAN_RFLM = DISABLE;
    can.CAN_TXFP = DISABLE;
    //can.CAN_Mode = CAN_Mode_LoopBack;
    can.CAN_Mode = CAN_Mode_Normal;
    can.CAN_SJW = CAN_SJW_1tq;
    can.CAN_BS1 = CAN_BS1_6tq;
    can.CAN_BS2 = CAN_BS2_8tq;
    can.CAN_Prescaler = 16;
    CAN_Init(CAN1, &can);

    filter.CAN_FilterNumber = 0;
    filter.CAN_FilterMode = CAN_FilterMode_IdMask;
    filter.CAN_FilterScale = CAN_FilterScale_32bit;
    filter.CAN_FilterIdHigh = 0x0000;
    filter.CAN_FilterIdLow = 0x0000;
    filter.CAN_FilterMaskIdHigh = 0x0000;
    filter.CAN_FilterMaskIdLow = 0x0000;
    filter.CAN_FilterFIFOAssignment = 0;
    filter.CAN_FilterActivation = ENABLE;
    CAN_FilterInit(&filter);

    CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

    nvic.NVIC_IRQChannel = CAN1_RX0_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0x0;
    nvic.NVIC_IRQChannelSubPriority = 0x0;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    nvic.NVIC_IRQChannel = CAN1_TX_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0x0;
    nvic.NVIC_IRQChannelSubPriority = 0x0;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
}

uint32_t canSend(uint8_t *data, uint32_t length, uint8_t sender)
{
    CanTxQueue *q = newTxQueue(data, length, sender);
    if (!q)
        return 0;
    if (!txQueue) {
        txQueue = q;
        onCanDataSent(0, 0);
    } else {
        CanTxQueue *tmp = txQueue;
        while (tmp->next)
            tmp = tmp->next;
        tmp->next = q;
    }
    return length;
}

//------------------------------------------------------------------------------

static CanTxQueue *newTxQueue(uint8_t *data, uint32_t length, uint8_t sender)
{
    CanTxQueue *q = intMalloc(sizeof(CanTxQueue));
    if (!q)
        return 0;
    q->next = 0;
    q->mess = intMalloc(length);
    if (!q->mess) {
        intFree(q);
        return 0;
    }
    memcpy(q->mess, data, length);
    q->length = length;
    q->sent = 0;
    q->sender = sender;
    return q;
}

static void onCanDataSent(uint8_t error, uint8_t sent)
{
    CanTxQueue *q = txQueue;
    if (!q)
        return;

    if (error) {
        // skip message(s)
        // TODO check error code
        txQueue = q->next;
        intFree(q->mess);
        intFree(q);
        q = txQueue;
        if (!q)
            return;
    }

    q->sent += sent;
    if (q->sent == q->length) {
        txQueue = q->next;
        intFree(q->mess);
        intFree(q);
        q = txQueue;
        if (!q)
            return;
    }

    uint32_t length = q->length - q->sent;
    if (length > 8)
        length = 8;

    CanExtHeader *head = (CanExtHeader *)&txMess.ExtId;
    head->complex = q->length > 8;
    head->count = (q->length + 7) / 8;
    head->number = q->sent / 8;
    head->id = q->sender;

    txMess.StdId = 0x00;
    txMess.RTR = CAN_RTR_DATA;
    txMess.IDE = CAN_ID_EXT;
    txMess.DLC = length;
    memcpy(txMess.Data, (uint8_t *)q->mess + q->sent, length);

    CAN_ClearITPendingBit(CAN1, CAN_IT_LEC);
    CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
    CAN_Transmit(CAN1, &txMess);
}

static void onCanDataReceived(void)
{
    CAN_Receive(CAN1, CAN_FIFO0, &rxMess);

    if (rxMess.IDE != CAN_ID_EXT)
        return;

    CanExtHeader *head = (CanExtHeader *)&rxMess.ExtId;
    if (!head->complex) {
        void *mess = intMalloc(rxMess.DLC);
        if (mess) {
            memcpy(mess, rxMess.Data, rxMess.DLC);
            pushQueue((pulf3ul)onCanMessageReceived, head->id, (uint32_t)mess, 0);
        }
        return;
    }

    uint8_t id = head->id;
    CanRxQueue *rx = 0;

    if (rxQueue) {
        rx = rxQueue;
        while (rx && rx->sender != id)
            rx = rx->next;
    }

    if (!head->number) {
        if (!rx) {
            rx = intMalloc(sizeof(CanRxQueue));
            if (!rx)
                return;
            rx->next = 0;
            rx->sender = id;
            rx->length = head->count * 8; // max possible length
            rx->mess = intMalloc(rx->length);
            if (!rx->mess) {
                intFree(rx);
                return;
            }
            rx->next = rxQueue;
            rxQueue = rx;
        }
    }

    if (!rx)
        return;

    memcpy((uint8_t *)rx->mess + 8 * head->number, rxMess.Data, rxMess.DLC);

    if (head->number == (head->count - 1)) {
        rx->length -= 8 - rxMess.DLC; // correct message length
        pushQueue((pulf3ul)onCanMessageReceived, head->id, (uint32_t)rx->mess, 0);
        if (rx == rxQueue) {
            rxQueue = rx->next;
        } else {
            CanRxQueue *prev = rxQueue;
            while (prev->next != rx)
                prev = prev->next;
            prev->next = rx->next;
        }
        intFree(rx);
    }
}

//------------------------------------------------------------------------------

void CAN1_TX_IRQHandler(void)
{
    if (CAN_GetITStatus(CAN1, CAN_IT_TME)) {
        CAN_ClearITPendingBit(CAN1, CAN_IT_TME);
        CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
        uint8_t err = CAN_GetLastErrorCode(CAN1);
        pushQueue((pulf3ul)onCanDataSent, err, txMess.DLC, 0);
    }
}

void CAN1_RX0_IRQHandler(void)
{
    if (CAN_GetITStatus(CAN1, CAN_IT_FMP0)) {
        CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
        onCanDataReceived();
        CAN_FIFORelease(CAN1, CAN_FIFO0);
    }
}

//------------------------------------------------------------------------------
