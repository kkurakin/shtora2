//
//

#ifndef __CAN_H
#define __CAN_H

#include <stdint.h>

#if defined(DISCOVERY_MODE)

#define CAN_GPIO GPIOD
#define CAN_GPIO_PERIPH RCC_AHB1Periph_GPIOD
#define CAN_GPIO_PIN_TX GPIO_Pin_1
#define CAN_GPIO_PIN_RX GPIO_Pin_0
#define CAN_GPIO_PIN_SOURCE_TX GPIO_PinSource1
#define CAN_GPIO_PIN_SOURCE_RX GPIO_PinSource0

#else

//#error Real board mode to be checked

#define CAN_GPIO GPIOA
#define CAN_GPIO_PERIPH RCC_AHB1Periph_GPIOA
#define CAN_GPIO_PIN_TX GPIO_Pin_12
#define CAN_GPIO_PIN_RX GPIO_Pin_11
#define CAN_GPIO_PIN_SOURCE_TX GPIO_PinSource12
#define CAN_GPIO_PIN_SOURCE_RX GPIO_PinSource12

#endif

typedef struct {
    void *next;
    void *mess;
    uint32_t length;
    uint32_t sent;
    uint8_t sender;
} CanTxQueue;

typedef struct {
    void *next;
    uint8_t sender;
    void *mess;
    uint32_t length;
} CanRxQueue;

typedef struct {
    uint32_t number: 8;
    uint32_t count: 8;
    uint32_t id: 8;
    uint32_t complex: 1;
    uint32_t reserved: 7;
} CanExtHeader;

void initCanHW(void);
uint32_t canSend(uint8_t *data, uint32_t length, uint8_t sender);

#endif
