//
//

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "discovery.h"
#include "queue.h"

//-----------------------------------------------------------------------------

typedef struct {
    uint16_t pin;
    GPIO_TypeDef *port;
    uint32_t clk;
} LedData;

static LedData leds[LED_COUNT] = {
    {GPIO_Pin_12, GPIOD, RCC_AHB1Periph_GPIOD},
    {GPIO_Pin_13, GPIOD, RCC_AHB1Periph_GPIOD},
    {GPIO_Pin_14, GPIOD, RCC_AHB1Periph_GPIOD},
    {GPIO_Pin_15, GPIOD, RCC_AHB1Periph_GPIOD}
};

//-----------------------------------------------------------------------------

void initLeds(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    for (int i = 0; i < LED_COUNT; i++) {
        RCC_AHB1PeriphClockCmd(leds[i].clk, ENABLE);
        GPIO_InitStructure.GPIO_Pin = leds[i].pin;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(leds[i].port, &GPIO_InitStructure);
    }
}

void setLed(Led led, int state)
{
    if (state)
        leds[led].port->BSRRL = leds[led].pin;
    else
        leds[led].port->BSRRH = leds[led].pin;
}

void flashLed(Led led, int time, int state)
{
    if (!state) {
        setLed(led, 1);
        setTimeout(time, (pulf3ul)flashLed, led, time, 1);
    } else {
        setLed(led, 0);
    }
}

//-----------------------------------------------------------------------------
