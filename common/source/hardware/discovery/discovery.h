//
//

#ifndef __DISCOVERY_H
#define __DISCOVERY_H

typedef enum {
    LED_1,
    LED_2,
    LED_3,
    LED_4,
    LED_COUNT
} Led;

void initLeds(void);
void setLed(Led led, int state);
void flashLed(Led led, int time, int state);

#endif
