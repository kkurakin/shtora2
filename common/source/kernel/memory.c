//
//

#include <intrinsics.h>
#include "memory.h"

//------------------------------------------------------------------------------

uint32_t freeMemory = 1000;

//------------------------------------------------------------------------------

void *intMalloc(uint32_t size)
{
    __istate_t state = __get_interrupt_state();
    __disable_interrupt();

    void *ptr = malloc(size);

    freeMemory -= *((uint32_t *)ptr - 1);

    __set_interrupt_state(state);

    return ptr;
}

void *intRealloc(void *ptr, uint32_t size)
{
    __istate_t state = __get_interrupt_state();
    __disable_interrupt();

    ptr = realloc(ptr, size);

    __set_interrupt_state(state);

    return ptr;
}

void intFree(void *ptr)
{
    __istate_t state = __get_interrupt_state();
    __disable_interrupt();

    freeMemory += *((uint32_t *)ptr - 1);

    free(ptr);

    __set_interrupt_state(state);
}

//------------------------------------------------------------------------------
