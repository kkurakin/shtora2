//
//

#ifndef __MEMORY_H
#define __MEMORY_H

#include <stdint.h>
#include <stdlib.h>

extern uint32_t freeMemory;

void *intMalloc(uint32_t size);
void *intRealloc(void *ptr, uint32_t size);
void intFree(void *ptr);

#endif
