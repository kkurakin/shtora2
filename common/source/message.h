//
//

#ifndef __MESSAGE_H
#define __MESSAGE_H

#include <stdint.h>

typedef enum {
    ADDR_NULL = 0x00,
    ADDR_EXT_CTRL = 0x10,
    ADDR_RC_PULT = 0x20,
    ADDR_CTRL_MODULE = 0x30,
    ADDR_LITERA_1 = 0x40,
    ADDR_LITERA_2 = 0x50,
    ADDR_LITERA_3 = 0x60,
    ADDR_LITERA_4 = 0x70,
    ADDR_LITERA_5 = 0x80,
    ADDR_BROADCAST = 0xFD
} Address;

typedef enum {
    CMD_CHANGE_PARAM = 0x25,
    CMD_EXECUTED = 0x40,
    CMD_NOT_EXECUTED = 0x42,
    CMD_ANS_SIGNAL = 0x44,
    CMD_ANS_PARAM = 0x45,
    CMD_ASK_SIGNAL = 0xA4,
    CMD_ASK_PARAM = 0xA5,
    CMD_SET_SIGNAL = 0xC4,
    CMD_SET_PARAM = 0xC5
} Command;

typedef enum {
    SIGNAL_AFU_STATE = 0x16,
    SIGNAL_IRP_STATE = 0x18,
    SIGNAL_RAD_CTRL = 0x20
} SignalType;

typedef enum {
    PARAM_RAD_PROHIBITION = 0x30,
    PARAM_TEMPERATURE = 0x31,
    PARAM_AKB_MODE = 0x40,
    PARAM_AKB_CHARGE = 0x41,
    PARAM_MODULE_LITERA = 0x94,
    PARAM_MODULE_SLOT = 0x95,
    PARAM_MODULE_STATE = 0x96
} ParamType;

typedef enum {
    AKB_CHARGE_LOW = 0,
    AKB_CHARGE_25_ = 25,
    AKB_CHARGE_50_ = 50,
    AKB_CHARGE_75_ = 75,
    AKB_CHARGE_100_ = 100
} AkbCharge;

#pragma pack(1)
typedef struct {
    uint8_t dest;
    uint8_t length;
    uint8_t number;
    uint8_t command;
    uint8_t data[251];
} CanMessage;

typedef struct {
    uint16_t dest;
    uint8_t length;
    uint16_t sender;
    uint8_t number;
    uint8_t command;
    uint8_t data[248];
} EthMessage;
#pragma pack()

void onCanMessageReceived(uint8_t sender, CanMessage *mess);
void onEthernetMessageReceived(uint8_t *buff, uint32_t length);

#endif
