//
//

#ifndef __CLOCK_H
#define __CLOCK_H

#include <stdint.h>

typedef struct {
    uint32_t ticks10ms;
    uint8_t f1ms: 1;
    uint8_t f10ms: 1;
} SysFlag;

extern SysFlag sysFlag;

void initSysTicks(void);
uint32_t currentTime(void);
void delay(uint32_t value);

#endif
