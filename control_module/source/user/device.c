//
//

#include <string.h>
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_adc.h"
#include "device.h"
#include "queue.h"
#include "clock.h"
#include "can.h"
#include "message.h"
#include "memory.h"
#include "netconf.h"
#include "json.h"
#include "stm32f4x7_eth_bsp.h"
#if defined(DISCOVERY_MODE)
#include "discovery.h"
#endif

//------------------------------------------------------------------------------

static uint8_t canAddress = ADDR_CTRL_MODULE;
static uint8_t akbMode = 1;
static uint32_t akbCharge = 0;
static uint8_t convBuff[256];

static void initHW(void);
static void indicateOperability(uint32_t state);
static void updateAKBMode(void);
static void updateAKBCharge(void);
static CanMessage buildReplyCanMessage(uint8_t sender, CanMessage *mess, uint8_t length);
static EthMessage buildReplyEthMessage(EthMessage *mess, uint8_t length);
static CanMessage convertMessageEth2Can(EthMessage *eth);
static EthMessage convertMessageCan2Eth(CanMessage *can, uint8_t sender);

//------------------------------------------------------------------------------

void initDevice(void)
{
    initHW();

    initEthernet();

    indicateOperability(1);
}

void process10ms(void)
{
    static int counter = 0;

    if (!counter) {
        updateAKBMode();
        updateAKBCharge();
    }

    if (++counter == 10)
        counter = 0;

    ethernetHandler(currentTime());
}

void onCanMessageReceived(uint8_t sender, CanMessage *mess)
{
#if defined(DISCOVERY_MODE)
    flashLed(LED_4, 200, 0);
#endif

    if (mess->dest == ADDR_EXT_CTRL) {
        uint32_t (*sendMessage)(uint8_t *, uint32_t);
#if defined(DISCOVERY_MODE)
        uint32_t txVCPData(uint8_t *buff, uint32_t length);
        sendMessage = txVCPData;
#else
        sendMessage = ethernetSend;
#endif
        EthMessage m = convertMessageCan2Eth(mess, sender);
        ethMessage2Json((char *)convBuff, &m);
        //sendMessage((uint8_t *)&m, m.length);
        sendMessage(convBuff, strlen((const char *)convBuff));
        intFree(mess);
        return;
    }

    if (mess->dest == canAddress || mess->dest == ADDR_BROADCAST) {
        switch (mess->command) {
        case CMD_ASK_SIGNAL:
            break;
        case CMD_ASK_PARAM:
            if (mess->data[0] == PARAM_AKB_MODE) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_PARAM;
                r.data[0] = PARAM_AKB_MODE;
                r.data[1] = akbMode;
                canSend((uint8_t *)&r, r.length, canAddress);
            } else if (mess->data[0] == PARAM_AKB_CHARGE) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_PARAM;
                r.data[0] = PARAM_AKB_CHARGE;
                r.data[1] = akbCharge;
                canSend((uint8_t *)&r, r.length, canAddress);
            }
            break;
        case CMD_SET_SIGNAL:
            break;
        case CMD_SET_PARAM:
            break;
        }
    }

    intFree(mess);
}

void onEthernetMessageReceived(uint8_t *buff, uint32_t length)
{
    EthMessage *mess = (EthMessage *)convBuff;
    json2EthMessage(mess, (const char *)buff);

    if (mess->length) {
        if (mess->dest == ADDR_CTRL_MODULE) {
            uint32_t (*sendMessage)(uint8_t *, uint32_t);
    #if defined(DISCOVERY_MODE)
            uint32_t txVCPData(uint8_t *buff, uint32_t length);
            sendMessage = txVCPData;
    #else
            sendMessage = ethernetSend;
    #endif
            switch (mess->command) {
            case CMD_ASK_PARAM:
                if (mess->data[0] == PARAM_AKB_MODE) {
                    EthMessage r = buildReplyEthMessage(mess, 9);
                    r.command = CMD_ANS_PARAM;
                    r.data[0] = PARAM_AKB_MODE;
                    r.data[1] = akbMode;
                    ethMessage2Json((char *)convBuff, &r);
                    //sendMessage((uint8_t *)&r, r.length);
                    sendMessage(convBuff, strlen((const char *)convBuff));
                } else if (mess->data[0] == PARAM_AKB_CHARGE) {
                    EthMessage r = buildReplyEthMessage(mess, 9);
                    r.command = CMD_ANS_PARAM;
                    r.data[0] = PARAM_AKB_CHARGE;
                    r.data[1] = akbCharge;
                    ethMessage2Json((char *)convBuff, &r);
                    //sendMessage((uint8_t *)&r, r.length);
                    sendMessage(convBuff, strlen((const char *)convBuff));
                }
                break;
            }
        } else {
            CanMessage m = convertMessageEth2Can(mess);
            canSend((uint8_t *)&m, m.length, ADDR_EXT_CTRL);
        }
    }

    intFree(buff);
}

#if defined(DISCOVERY_MODE)
void onVCPDataReceived(uint8_t *buff, uint32_t length)
{
    onEthernetMessageReceived(buff, length);
}
#endif

//------------------------------------------------------------------------------

static void initHW(void)
{
    initCanHW();
#if !defined(DISCOVERY_MODE)
    ETH_BSP_Config();
#endif

    GPIO_InitTypeDef gpio;
    ADC_InitTypeDef adc;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_6;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_OType = GPIO_OType_OD;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN; // FIXME default state
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio);

    // operability indication
    gpio.GPIO_Pin = GPIO_Pin_15;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio);

    // ADC
    gpio.GPIO_Pin = GPIO_Pin_0;
    gpio.GPIO_Mode = GPIO_Mode_AN;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOC, &gpio);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    ADC_DeInit();

    adc.ADC_ContinuousConvMode = DISABLE;
    adc.ADC_Resolution = ADC_Resolution_12b;
    adc.ADC_ScanConvMode = DISABLE;
    adc.ADC_ExternalTrigConv = ADC_ExternalTrigConvEdge_None;
    adc.ADC_DataAlign = ADC_DataAlign_Right;

    ADC_Init(ADC1, &adc);
    ADC_Cmd(ADC1, ENABLE);
}

static void indicateOperability(uint32_t state)
{
    if (state)
        GPIO_SetBits(GPIOB, GPIO_Pin_15);
    else
        GPIO_ResetBits(GPIOB, GPIO_Pin_15);
    setTimeout(250, (pulf3ul)indicateOperability, !state, 0, 0);
}

static void updateAKBMode(void)
{
    uint8_t value = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6);
    if (!value) {
        // 220 V
        GPIO_SetBits(GPIOC, GPIO_Pin_7);
        akbMode = 0;
    } else {
        // external AKB
        GPIO_ResetBits(GPIOC, GPIO_Pin_7);
        akbMode = 1;
    }
}

static void turnExternalAKBPowerSupply(void)
{
    GPIO_SetBits(GPIOC, GPIO_Pin_8);
}

static void updateAKBCharge(void)
{
    // FIXME TODO ADC parameters, channel etc.
    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_3Cycles);
    ADC_SoftwareStartConv(ADC1);
    while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET) ;
    uint32_t value = ADC_GetConversionValue(ADC1);

    // convert to charge level
    if (value > AKB_CHARGE_75)
        akbCharge = 75;
    else if (value > AKB_CHARGE_50)
        akbCharge = 50;
    else if (value > AKB_CHARGE_25)
        akbCharge = 25;
    else
        akbCharge = 0;

    if (value < AKB_VOLTAGE_2_4_V) {
        clrTimeout((pulf3ul)turnExternalAKBPowerSupply);
        GPIO_ResetBits(GPIOC, GPIO_Pin_8);
        GPIO_ResetBits(GPIOC, GPIO_Pin_9);
    } else {
        if (!GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_9)) {
            GPIO_SetBits(GPIOC, GPIO_Pin_9);
            setTimeout(300, (pulf3ul)turnExternalAKBPowerSupply, 0, 0, 0);
        }
    }
}

static CanMessage buildReplyCanMessage(uint8_t sender, CanMessage *mess, uint8_t length)
{
    CanMessage reply;
    reply.dest = sender;
    reply.length = length;
    reply.number = mess->number ? (mess->number + 8) : 0x00;
    return reply;
}

static EthMessage buildReplyEthMessage(EthMessage *mess, uint8_t length)
{
    EthMessage reply;
    reply.dest = mess->sender;
    reply.length = length;
    reply.sender = mess->dest;
    reply.number = mess->number ? (mess->number + 8) : 0x00;
    return reply;
}

static CanMessage convertMessageEth2Can(EthMessage *eth)
{
    CanMessage can;
    can.dest = eth->dest;
    can.length = eth->length - 3;
    can.number = eth->number;
    can.command = eth->command;
    memcpy(can.data, eth->data, eth->length - 7);
    return can;
}

static EthMessage convertMessageCan2Eth(CanMessage *can, uint8_t sender)
{
    EthMessage eth;
    eth.dest = can->dest;
    eth.length = can->length + 3;
    eth.sender = sender;
    eth.number = can->number;
    eth.command = can->command;
    memcpy(eth.data, can->data, can->length - 4);
    return eth;
}

//------------------------------------------------------------------------------
