//
//

#ifndef __DEVICE_H
#define __DEVICE_H

// FIXME TODO define AKB charge levels
#define AKB_CHARGE_75 3800
#define AKB_CHARGE_50 3600
#define AKB_CHARGE_25 3400
// FIXME TODO define value for 2.4V
#define AKB_VOLTAGE_2_4_V AKB_CHARGE_25

void initDevice(void);
void process10ms(void);

#endif
