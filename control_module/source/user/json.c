//
//

#include <stdlib.h>
#include <string.h>
#include "json.h"
#include "cjson.h"
#include "message.h"

//------------------------------------------------------------------------------

static const struct {
    const char *name;
    Address address;
} devices[] = {
    {EXT_CTRL_DEV, ADDR_EXT_CTRL},
    {RC_PULT_DEV, ADDR_RC_PULT},
    {CTRL_MODULE_DEV, ADDR_CTRL_MODULE},
    {LITERA1_DEV, ADDR_LITERA_1},
    {LITERA2_DEV, ADDR_LITERA_2},
    {LITERA3_DEV, ADDR_LITERA_3},
    {LITERA4_DEV, ADDR_LITERA_4},
    {LITERA5_DEV, ADDR_LITERA_5},
    {BROADCAST_DEV, ADDR_BROADCAST}
};

static const struct {
    const char *name;
    Command command;
} commands[] = {
    {"changeParam", CMD_CHANGE_PARAM},
    {"ansSignal", CMD_ANS_SIGNAL},
    {"ansParam", CMD_ANS_PARAM},
    {"askSignal", CMD_ASK_SIGNAL},
    {"askParam", CMD_ASK_PARAM},
    {"setSignal", CMD_SET_SIGNAL},
    {"setParam", CMD_SET_PARAM}
};

typedef struct {
    const char *name;
    uint8_t value;
} ParamsData;

static const ParamsData signalParams[] = {
    {"afuState", SIGNAL_AFU_STATE},
    {"irpState", SIGNAL_IRP_STATE},
    {"radControl", SIGNAL_RAD_CTRL},
    {0, 0}
};

static const ParamsData paramParams[] = {
    {"radProhibition", PARAM_RAD_PROHIBITION},
    {"akbMode", PARAM_AKB_MODE},
    {"akbCharge", PARAM_AKB_CHARGE},
    {0, 0}
};

static const struct {
    Command command;
    const ParamsData *data;
} params[] = {
    {CMD_CHANGE_PARAM, paramParams},
    {CMD_ANS_SIGNAL, signalParams},
    {CMD_ANS_PARAM, paramParams},
    {CMD_ASK_SIGNAL, signalParams},
    {CMD_ASK_PARAM, paramParams},
    {CMD_SET_SIGNAL, signalParams},
    {CMD_SET_PARAM, paramParams}
};

static uint16_t destFromJson(cJSON *item);
static const char *deviceName(uint16_t addr);
static uint8_t commandFromJson(cJSON *item);
static const char *commandName(uint8_t cmd);
static uint8_t paramFromJson(uint8_t command, cJSON *item);
static const char *paramName(uint8_t command, uint8_t param);

//------------------------------------------------------------------------------

void json2EthMessage(EthMessage *msg, const char *text)
{
    msg->length = 0;

    cJSON *json = cJSON_Parse(text);
    if (!json)
        return;

    cJSON *dev = cJSON_GetObjectItem(json, "device");
    if (!dev || !cJSON_IsString(dev))
        return;
    cJSON *num = cJSON_GetObjectItem(json, "number");
    if (!num || !cJSON_IsNumber(num))
        return;
    cJSON *cmd = cJSON_GetObjectItem(json, "command");
    if (!cmd || !cJSON_IsString(cmd))
        return;
    cJSON *par = cJSON_GetObjectItem(json, "parameter");
    if (!par || !cJSON_IsString(par))
        return;
    cJSON *dat = cJSON_GetObjectItem(json, "data");
    if (!dat || !cJSON_IsArray(dat))
        return;

    msg->dest = destFromJson(dev);
    msg->sender = ADDR_EXT_CTRL;
    msg->number = num->valueint;
    msg->command = commandFromJson(cmd);
    msg->data[0] = paramFromJson(msg->command, par);

    for (int i = 0; i < cJSON_GetArraySize(dat); i++) {
        cJSON *d = cJSON_GetArrayItem(dat, i);
        if (!cJSON_IsNumber(d))
            return;
        msg->data[i + 1] = d->valueint;
    }

    msg->length = 8 + cJSON_GetArraySize(dat);

    cJSON_Delete(json);
}

void ethMessage2Json(char *buff, const EthMessage *msg)
{
    buff[0] = 0;

    const char *devName = deviceName(msg->sender);
    if (!devName)
        return;
    const char *cmdName = commandName(msg->command);
    if (!cmdName)
        return;
    const char *parName = paramName(msg->command, msg->data[0]);
    if (!parName)
        return;

    cJSON *obj = cJSON_CreateObject();
    if (!obj)
        return;

    cJSON *dev = cJSON_CreateString(devName);
    if (!dev) {
        cJSON_Delete(obj);
        return;
    }
    cJSON_AddItemToObject(obj, "device", dev);

    cJSON *num = cJSON_CreateNumber(msg->number);
    if (!num) {
        cJSON_Delete(obj);
        return;
    }
    cJSON_AddItemToObject(obj, "number", num);

    cJSON *cmd = cJSON_CreateString(cmdName);
    if (!cmd) {
        cJSON_Delete(obj);
        return;
    }
    cJSON_AddItemToObject(obj, "command", cmd);

    cJSON *par = cJSON_CreateString(parName);
    if (!par) {
        cJSON_Delete(obj);
        return;
    }
    cJSON_AddItemToObject(obj, "parameter", par);

    cJSON *data = cJSON_CreateArray();
    if (!data) {
        cJSON_Delete(obj);
        return;
    }
    cJSON_AddItemToObject(obj, "data", data);
    for (int i = 0; i < msg->length - 8; i++) {
        cJSON *d = cJSON_CreateNumber(msg->data[i + 1]);
        if (!d) {
            cJSON_Delete(obj);
            return;
        }
        cJSON_AddItemToArray(data, d);
    }

    char *text = cJSON_PrintUnformatted(obj);
    if (text) {
        strcpy(buff, text);
        free(text);
    }

    cJSON_Delete(obj);
}

//------------------------------------------------------------------------------

static uint16_t destFromJson(cJSON *item)
{
    for (int i = 0; i < sizeof(devices) / sizeof(devices[0]); i++) {
        if (!strcmp(devices[i].name, item->valuestring))
            return devices[i].address;
    }
    return 0x0000;
}

static const char *deviceName(uint16_t addr)
{
    for (int i = 0; i < sizeof(devices) / sizeof(devices[0]); i++) {
        if (devices[i].address == addr)
            return devices[i].name;
    }
    return 0;
}

static uint8_t commandFromJson(cJSON *item)
{
    for (int i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
        if (!strcmp(commands[i].name, item->valuestring))
            return commands[i].command;
    }
    return 0x00;
}

static const char *commandName(uint8_t cmd)
{
    for (int i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
        if (commands[i].command == cmd)
            return commands[i].name;
    }
    return 0;
}

static uint8_t paramFromJson(uint8_t command, cJSON *item)
{
    for (int i = 0; i < sizeof(params) / sizeof(params[0]); i++) {
        if (params[i].command == command) {
            const ParamsData *data = params[i].data;
            while (data->name) {
                if (!strcmp(data->name, item->valuestring))
                    return data->value;
                data++;
            }
        }
    }
    return 0x00;
}

static const char *paramName(uint8_t command, uint8_t param)
{
    for (int i = 0; i < sizeof(params) / sizeof(params[0]); i++) {
        if (params[i].command == command) {
            const ParamsData *data = params[i].data;
            while (data->name) {
                if (data->value == param)
                    return data->name;
                data++;
            }
        }
    }
    return 0;
}

//------------------------------------------------------------------------------
