//
//

#ifndef __JSON_H
#define __JSON_H

#include "message.h"

#define EXT_CTRL_DEV "externalControl"
#define RC_PULT_DEV "rcPult"
#define CTRL_MODULE_DEV "controlModule"
#define LITERA1_DEV "litera1"
#define LITERA2_DEV "litera2"
#define LITERA3_DEV "litera3"
#define LITERA4_DEV "litera4"
#define LITERA5_DEV "litera5"
#define BROADCAST_DEV "broadcast"

void json2EthMessage(EthMessage *msg, const char *text);
void ethMessage2Json(char *buff, const EthMessage *msg);

#endif
