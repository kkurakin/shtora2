#-------------------------------------------------
#
# Project created by QtCreator 2019-07-15T11:19:30
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = external_control
TEMPLATE = app

SOURCES += main.cpp\
	mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

QSERIAL_LIB_DIR = qtserialport
QSERIAL_LIB = -lqt5serialport
CONFIG (debug, debug|release) {
	QSERIAL_LIB = $$join(QSERIAL_LIB,,,d)
}
INCLUDEPATH += ./$$QSERIAL_LIB_DIR
LIBS += -L./$$QSERIAL_LIB_DIR $$QSERIAL_LIB
LIBS += -lsetupapi
