//
//

#include <QDateTime>
#include <QJsonDocument>
#include <QMenu>
#include <QSettings>
#include <QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "..\common\source\message.h"

//------------------------------------------------------------------------------

#define EXT_CTRL_DEV "externalControl"
#define RC_PULT_DEV "rcPult"
#define CTRL_MODULE_DEV "controlModule"
#define LITERA1_DEV "litera1"
#define LITERA2_DEV "litera2"
#define LITERA3_DEV "litera3"
#define LITERA4_DEV "litera4"
#define LITERA5_DEV "litera5"
#define BROADCAST_DEV "broadcast"

static const struct {
    const char *name;
    Address address;
} devices[] = {
    {EXT_CTRL_DEV, ADDR_EXT_CTRL},
    {RC_PULT_DEV, ADDR_RC_PULT},
    {CTRL_MODULE_DEV, ADDR_CTRL_MODULE},
    {LITERA1_DEV, ADDR_LITERA_1},
    {LITERA2_DEV, ADDR_LITERA_2},
    {LITERA3_DEV, ADDR_LITERA_3},
    {LITERA4_DEV, ADDR_LITERA_4},
    {LITERA5_DEV, ADDR_LITERA_5},
    {BROADCAST_DEV, ADDR_BROADCAST}
};

static const struct {
    const char *name;
    Command command;
} commands[] = {
    {"changeParam", CMD_CHANGE_PARAM},
    {"ansSignal", CMD_ANS_SIGNAL},
    {"ansParam", CMD_ANS_PARAM},
    {"askSignal", CMD_ASK_SIGNAL},
    {"askParam", CMD_ASK_PARAM},
    {"setSignal", CMD_SET_SIGNAL},
    {"setParam", CMD_SET_PARAM}
};

typedef struct {
    const char *name;
    uint8_t value;
} ParamsData;

static const ParamsData signalParams[] = {
    {"afuState", SIGNAL_AFU_STATE},
    {"irpState", SIGNAL_IRP_STATE},
    {"radControl", SIGNAL_RAD_CTRL},
    {0, 0}
};

static const ParamsData paramParams[] = {
    {"radProhibition", PARAM_RAD_PROHIBITION},
    {"akbMode", PARAM_AKB_MODE},
    {"akbCharge", PARAM_AKB_CHARGE},
    {0, 0}
};

static const struct {
    Command command;
    const ParamsData *data;
} params[] = {
    {CMD_CHANGE_PARAM, paramParams},
    {CMD_ANS_SIGNAL, signalParams},
    {CMD_ANS_PARAM, paramParams},
    {CMD_ASK_SIGNAL, signalParams},
    {CMD_ASK_PARAM, paramParams},
    {CMD_SET_SIGNAL, signalParams},
    {CMD_SET_PARAM, paramParams}
};

//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	transport(0)
{
	ui->setupUi(this);
	setWindowFlags(windowFlags() ^ Qt::WindowMaximizeButtonHint);

	QSettings set(QApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat);
	set.beginGroup(objectName());
	restoreGeometry(set.value("geometry").toByteArray());
	ui->teJson->setText(set.value("json").toString());
	set.endGroup();

	prepareGui();
}

MainWindow::~MainWindow()
{
	QSettings set(QApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat);
	set.beginGroup(objectName());
	set.setValue("geometry", saveGeometry());
	set.setValue("json", ui->teJson->toPlainText());
	set.endGroup();

	delete ui;
}

//------------------------------------------------------------------------------

void MainWindow::on_lwDevices_itemClicked(QListWidgetItem *item)
{
	if (item)
		ui->leDest->setText(item->data(Qt::UserRole).toString());
}

void MainWindow::on_lwCommands_itemClicked(QListWidgetItem *item)
{
	if (item) {
		QByteArray data = item->data(Qt::UserRole).toByteArray();
		ui->leCommand->setText(data.mid(0, 1).toHex().toUpper());
		ui->leData->setText(data.mid(1).toHex().toUpper());
	}
}

void MainWindow::on_tbExecute_clicked()
{
	if (!transport)
		return;

	QByteArray msg;

	msg.append(QByteArray::fromHex(ui->leDest->text().toLatin1()));
	msg.append(0x08); // will be corrected later
	msg.append(QByteArray::fromHex(ui->leSender->text().toLatin1()));
	msg.append(QByteArray::fromHex(ui->leNumber->text().toLatin1()));
	msg.append(QByteArray::fromHex(ui->leCommand->text().toLatin1()));
	msg.append(QByteArray::fromHex(ui->leData->text().toLatin1()));
	msg[2] = msg.length();

	transport->sendData(msg);
	log(msg);
}

void MainWindow::on_pExecute_clicked()
{
	QString text = ui->teJson->toPlainText();
	qDebug() << 1 << text;
	//QJsonDocument doc = QJsonDocument::fromJson(text.toLatin1());
	//text = doc.toJson(QJsonDocument::Compact);
	QByteArray msg(text.toLatin1());
	msg.append((char)0x00);
	transport->sendData(msg);
	ui->teReceived->clear();
}

void MainWindow::on_cbDevice_currentIndexChanged(int index)
{
	updateJsonWindow();
}

void MainWindow::on_cbNumber_currentIndexChanged(int index)
{
	updateJsonWindow();
}

void MainWindow::on_cbCommand_currentIndexChanged(int index)
{
	int cmd = ui->cbCommand->itemData(index).toInt();
	for (int i = 0; i < sizeof(params) / sizeof(params[0]); i++) {
		if (params[i].command == cmd) {
			const ParamsData *data = params[i].data;
			ui->cbParameter->clear();
			while (data->name) {
				ui->cbParameter->addItem(data->name, data->value);
				data++;
			}
		}
	}
	updateJsonWindow();
}

void MainWindow::on_cbParameter_currentIndexChanged(int index)
{
	updateJsonWindow();
}

//------------------------------------------------------------------------------

void MainWindow::onTransportMenuAboutToShow()
{
	QMenu *menu = transports->menu();
	QActionGroup *group = new QActionGroup(this);
	QAction *act;

	menu->clear();
	QList<QSerialPortInfo> devs = QSerialPortInfo::availablePorts();
	qSort(devs.begin(), devs.end(), [](const QSerialPortInfo &i1, const QSerialPortInfo &i2) {
		int com1 = i1.portName().remove("COM").toInt();
		int com2 = i2.portName().remove("COM").toInt();
		return com1 < com2;
	});
	for (int i = 0; i < devs.size(); i++) {
		act = new QAction(devs[i].portName(), this);
		act->setObjectName(devs[i].portName());
		act->setCheckable(true);
		connect(act, SIGNAL(triggered()), this, SLOT(onTransportSelected()));
		group->addAction(act);
		menu->addAction(act);
	}
	menu->addSeparator();

	act = new QAction("UDP", this);
	act->setObjectName("UDP");
	act->setCheckable(true);
	connect(act, SIGNAL(triggered()), this, SLOT(onTransportSelected()));
	group->addAction(act);
	menu->addAction(act);

	menu->addSeparator();

	act = new QAction(QString::fromUtf8("Выкл"), this);
	act->setObjectName(QString::fromUtf8("Выкл"));
	act->setCheckable(true);
	connect(act, SIGNAL(triggered()), this, SLOT(onTransportSelected()));
	group->addAction(act);
	menu->addAction(act);
}

void MainWindow::onTransportSelected()
{
	QObject *obj = QObject::sender();
	QString name(obj->objectName());

	transports->setText(name);

	delete transport;
	transport = 0;

	if (name == QString::fromUtf8("Выкл")) {
		return;
	} else if (name == "UDP") {
		transport = new TransportUdp(this);
	} else {
		transport = new TransportCom(name, this);
	}

	connect(transport, &Transport::onDataReady, [this](const QByteArray &data) {
		onDataReceived(data);
	});
}

void MainWindow::onDataReceived(const QByteArray &data)
{
	qDebug() << data;
	log(data);
	QByteArray tmp(data);
	tmp.append((char)0x00);
	ui->teReceived->setPlainText(tmp);
}

//------------------------------------------------------------------------------

void MainWindow::prepareGui()
{
	ui->twNoiseModules->setColumnWidth(0, 48);
	ui->twNoiseModules->setColumnWidth(1, 192);
	ui->twNoiseModules->setColumnWidth(2, 80);
	ui->twNoiseModules->setColumnWidth(3, 72);

	QMenu *menu = new QMenu(this);
	connect(menu, SIGNAL(aboutToShow()), this, SLOT(onTransportMenuAboutToShow()));
	transports = new QToolButton(this);
	transports->setText(QString::fromUtf8("Выкл"));
	transports->setFixedWidth(64);
	transports->setMenu(menu);
	transports->setPopupMode(QToolButton::InstantPopup);
	statusBar()->addWidget(transports);
#if 0
	static const struct {
		QString name;
		QString address;
	} devices[] = {
		{QString::fromUtf8("Литера 1"), "4000"},
		{QString::fromUtf8("Литера 2"), "5000"},
		{QString::fromUtf8("Литера 3"), "6000"},
		{QString::fromUtf8("Литера 4"), "7000"},
		{QString::fromUtf8("Литера 5"), "8000"},
		{QString::fromUtf8("Модуль управления"), "3000"},
		{QString::fromUtf8("Пульт ДУ"), "2000"}
	};

	for (size_t i = 0; i < sizeof(devices) / sizeof(devices[0]); i++) {
		QListWidgetItem *it = new QListWidgetItem(ui->lwDevices);
		it->setText(devices[i].name);
		it->setData(Qt::UserRole, devices[i].address);
	}

	static const struct {
		QString name;
		quint8 command;
		quint8 data;
	} commands[] = {
		{QString::fromUtf8("Запрос номера слота"), CMD_ASK_PARAM, PARAM_MODULE_SLOT},
		{QString::fromUtf8("Запрос исправности возбудителя"), CMD_ASK_SIGNAL, SIGNAL_AFU_STATE},
		{QString::fromUtf8("Запрос состояния излучения"), CMD_ASK_SIGNAL, SIGNAL_IRP_STATE},
		{QString::fromUtf8("Запрос запрета излучения"), CMD_ASK_PARAM, PARAM_RAD_PROHIBITION},
		{QString::fromUtf8("Запрос темперауры возбудителя"), CMD_ASK_PARAM, PARAM_TEMPERATURE},
	};

	for (size_t i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
		QListWidgetItem *it = new QListWidgetItem(ui->lwCommands);
		it->setText(commands[i].name);
		QByteArray data;
		data.append(commands[i].command);
		data.append(commands[i].data);
		it->setData(Qt::UserRole, data);
	}
#endif
	// JSON tab
	for (int i = 0; i < sizeof(devices) / sizeof(devices[0]); i++) {
		ui->cbDevice->addItem(devices[i].name, devices[i].address);
	}
	for (int i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
		ui->cbCommand->addItem(commands[i].name, commands[i].command);
	}
}

void MainWindow::updateJsonWindow()
{
	QString device = ui->cbDevice->currentText();
	QString number = ui->cbNumber->currentText();
	QString command = ui->cbCommand->currentText();
	QString parameter = ui->cbParameter->currentText();
	QString data = ui->leParamData->text();

	QString templ = "{\n\t\"device\": \"%1\",\n\t\"number\": %2,\n\t\"command\": \"%3\",\n\t\"parameter\": \"%4\",\n\t\"data\": [%5]\n\}";

	QString text = templ.arg(device).arg(number).arg(command).arg(parameter).arg(data);
	qDebug() << text;
	ui->teJson->setPlainText(text);
}

void MainWindow::log(const QByteArray &data)
{
	auto format = [](const QByteArray &data) {
		QString text = data.toHex().toUpper();
		text.insert(14, " ");
		text.insert(12, " ");
		text.insert(10, " ");
		text.insert(6, " ");
		text.insert(4, " ");
		return text;
	};
	QTreeWidgetItem *it = new QTreeWidgetItem();
	it->setText(0, QDateTime::currentDateTime().toString("hh:mm:ss.zzz"));
	it->setText(1, format(data));
	ui->twLogs->insertTopLevelItem(0, it);
}

//------------------------------------------------------------------------------

TransportCom::TransportCom(const QString &port, QObject *parent) : Transport(parent), serial(port)
{
	serial.setBaudRate(57600);
	serial.setParity(QSerialPort::NoParity);
	serial.setDataBits(QSerialPort::Data8);
	serial.setStopBits(QSerialPort::OneStop);
	connect(&serial, SIGNAL(readyRead()), SLOT(onReadyRead()));
	serial.setPortName(port);
	serial.open(QIODevice::ReadWrite);
}

void TransportCom::sendData(const QByteArray &data)
{
	serial.write(data);
}

void TransportCom::onReadyRead()
{
	QByteArray data = serial.readAll();
	emit onDataReady(data);
}

//------------------------------------------------------------------------------

TransportUdp::TransportUdp(QObject *parent) : Transport(parent)
{
	socket = new QUdpSocket(this);
	socket->bind(QHostAddress::Any, 2048);
	connect(socket, SIGNAL(readyRead()), this, SLOT(onDataReceived()));
}

void TransportUdp::sendData(const QByteArray &data)
{
	QHostAddress remoteAddr("192.168.0.104");
	quint16 remotePort = 2048;
	socket->writeDatagram(data, remoteAddr, remotePort);
}

void TransportUdp::onDataReceived()
{
	QHostAddress addr;
	QByteArray data(socket->pendingDatagramSize(), 0x00);

	socket->readDatagram(data.data(), data.size(), &addr);

	emit onDataReady(data);
}

//------------------------------------------------------------------------------
