#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolButton>
#include <QListWidgetItem>
#include <QUdpSocket>
#include "qtserialport/qserialport.h"
#include "qtserialport/qserialportinfo.h"

namespace Ui {
class MainWindow;
}

class Transport;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_lwDevices_itemClicked(QListWidgetItem *item);
	void on_lwCommands_itemClicked(QListWidgetItem *item);
	void on_tbExecute_clicked();
	void on_pExecute_clicked();
	void on_cbDevice_currentIndexChanged(int index);
	void on_cbNumber_currentIndexChanged(int index);
	void on_cbCommand_currentIndexChanged(int index);
	void on_cbParameter_currentIndexChanged(int index);

private slots:
	void onTransportMenuAboutToShow();
	void onTransportSelected();
	void onDataReceived(const QByteArray &data);

private:
	void prepareGui();
	void updateJsonWindow();
	void log(const QByteArray &data);

private:
	Ui::MainWindow *ui;
	QToolButton *transports;
	Transport *transport;
};

class Transport : public QObject
{
	Q_OBJECT
public:
	Transport(QObject *parent) : QObject(parent) {}
	virtual void sendData(const QByteArray &data) {Q_UNUSED(data);}

signals:
	void onDataReady(const QByteArray &data);
};

class TransportCom : public Transport
{
	Q_OBJECT
public:
	TransportCom(const QString &port, QObject *parent);
	void sendData(const QByteArray &data);

private slots:
	void onReadyRead();

private:
	QSerialPort serial;
};

class TransportUdp : public Transport
{
	Q_OBJECT
public:
	TransportUdp(QObject *parent);
	void sendData(const QByteArray &data);

private slots:
	void onDataReceived();

private:
	QUdpSocket *socket;
};

#endif // MAINWINDOW_H
