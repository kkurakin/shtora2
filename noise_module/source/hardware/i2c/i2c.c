//
//

#include "stm32f4xx_i2c.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "i2c.h"

//------------------------------------------------------------------------------

void initI2CHW(void)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
    RCC_AHB1PeriphClockCmd(I2C_GPIO_CLOCK, ENABLE);

    I2C_InitTypeDef i2c;
    GPIO_InitTypeDef gpio;

    i2c.I2C_ClockSpeed = 100000;
    i2c.I2C_Mode = I2C_Mode_I2C;
    i2c.I2C_DutyCycle = I2C_DutyCycle_2;
    i2c.I2C_OwnAddress1 = 0x15;
    i2c.I2C_Ack = I2C_Ack_Disable;
    i2c.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_Init(I2C1, &i2c);

    gpio.GPIO_Pin = I2C_GPIO_CLK_PIN | I2C_GPIO_DATA_PIN;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_OType = GPIO_OType_OD;
    gpio.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(I2C_GPIO, &gpio);

    GPIO_PinAFConfig(I2C_GPIO, I2C_GPIO_CLK_PIN_SRC, GPIO_AF_I2C1);
    GPIO_PinAFConfig(I2C_GPIO, I2C_GPIO_DATA_PIN_SRC, GPIO_AF_I2C1);

    I2C_Cmd(I2C1, ENABLE);
}

//------------------------------------------------------------------------------
