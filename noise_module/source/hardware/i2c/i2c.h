//
//

#ifndef __I2C_H
#define __I2C_H

#define I2C_GPIO_CLOCK RCC_AHB1Periph_GPIOB
#define I2C_GPIO GPIOB
#define I2C_GPIO_CLK_PIN GPIO_Pin_6
#define I2C_GPIO_CLK_PIN_SRC GPIO_PinSource6
#define I2C_GPIO_DATA_PIN GPIO_Pin_7
#define I2C_GPIO_DATA_PIN_SRC GPIO_PinSource7

void initI2CHW(void);

#endif
