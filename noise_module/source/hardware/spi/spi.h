//
//

#ifndef __SPI_H
#define __SPI_H

#include <stdint.h>

#define INDICATE_SPI_GPIO_CLK RCC_AHB1Periph_GPIOB
#define INDICATE_SPI_GPIO GPIOB
#define INDICATE_SPI_MISO_PIN GPIO_Pin_14
#define INDICATE_SPI_MISO_PIN_SRC GPIO_PinSource14
#define INDICATE_SPI_MOSI_PIN GPIO_Pin_15
#define INDICATE_SPI_MOSI_PIN_SRC GPIO_PinSource15
#define INDICATE_SPI_CLK_PIN GPIO_Pin_13
#define INDICATE_SPI_CLK_PIN_SRC GPIO_PinSource13
#define INDICATE_SPI_CS_PIN GPIO_Pin_12

void initSpiHW(void);
uint8_t spiSendByte(SPI_TypeDef* spi, uint8_t data);

#endif
