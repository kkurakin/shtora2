//
//

#ifndef __QUEUE_H
#define __QUEUE_H

#include <stdint.h>

typedef uint32_t (ulf3ul)(uint32_t, uint32_t, uint32_t);
typedef ulf3ul *pulf3ul;

typedef struct {
    pulf3ul func;
    uint32_t param[3];
} RecQ;

typedef struct {
    uint32_t time;
    RecQ rec;
} RecW;

int pushQueue(pulf3ul func, uint32_t param1, uint32_t param2, uint32_t param3);
void runQueue(void);
void checkTimeout(void);
void setTimeout(uint32_t time, pulf3ul func, uint32_t param1, uint32_t param2, uint32_t param3);
void clrTimeout(pulf3ul func);

#endif
