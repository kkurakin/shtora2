//
//

#ifndef __CAN_TYPES_H
#define __CAN_TYPES_H

#include <stdint.h>

typedef enum {
    CAN_ADDR_NULL = 0x00,
    CAN_ADDR_EXT_CTRL = 0x10,
    CAN_ADDR_RC_PULT = 0x20,
    CAN_ADDR_CTRL_MODULE = 0x30,
    CAN_ADDR_NOISE_MODULE_1 = 0x40,
    CAN_ADDR_NOISE_MODULE_2 = 0x50,
    CAN_ADDR_NOISE_MODULE_3 = 0x60,
    CAN_ADDR_NOISE_MODULE_4 = 0x70,
    CAN_ADDR_NOISE_MODULE_5 = 0x80,
    CAN_ADDR_BROADCAST = 0xFD
} CanAddress;

typedef enum {
    CAN_CMD_CHANGE_PARAM = 0x25,
    CAN_CMD_ANS_SIGNAL = 0xA4,
    CAN_CMD_ANS_PARAM = 0xA5,
    CAN_CMD_ASK_SIGNAL = 0xA4,
    CAN_CMD_ASK_PARAM = 0xA5,
    CAN_CMD_SET_SIGNAL = 0xC4,
    CAN_CMD_SET_PARAM = 0xC5
} CanCommand;

typedef enum {
    CAN_SIGNAL_AFU_STATE = 0x16,
    CAN_SIGNAL_RAD_STATE = 0x18,
    CAN_SIGNAL_RAD_CTRL = 0x20
} CanSignalType;

typedef enum {
    CAN_PARAM_RAD_PROHIBITION = 0x30,
    CAN_PARAM_MODULE_SLOT = 0x95,
    CAN_PARAM_MODULE_STATE = 0x96
} CanParamType;

#pragma pack(1)
typedef struct {
    uint8_t dest;
    uint8_t length;
    uint8_t number;
    uint8_t command;
    uint8_t data[251];
} CanMessage;
#pragma pack()

void onCanMessageReceived(uint8_t sender, CanMessage *mess);

#endif
