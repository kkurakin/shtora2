//
//

//#include <stdlib.h>
#include <string.h>
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_i2c.h"
#include "stm32f4xx_spi.h"
#include "device.h"
#include "queue.h"
#include "message.h"
#include "memory.h"
#include "can.h"
#include "i2c.h"
#include "spi.h"
#if defined(DISCOVERY_MODE)
#include "discovery.h"
#endif

//------------------------------------------------------------------------------

#if defined(NOISE_MODULE_1)
static uint8_t canAddress = ADDR_LITERA_1;
static const uint8_t litera = 1;
#elif defined(NOISE_MODULE_2)
static uint8_t canAddress = ADDR_LITERA_2;
static const uint8_t litera = 2;
#elif defined(NOISE_MODULE_3)
static uint8_t canAddress = ADDR_LITERA_3;
static const uint8_t litera = 3;
#elif defined(NOISE_MODULE_4)
static uint8_t canAddress = ADDR_LITERA_4;
static const uint8_t litera = 4;
#elif defined(NOISE_MODULE_5)
static uint8_t canAddress = ADDR_LITERA_5;
static const uint8_t litera = 5;
#else
#error No noise module type defined.
#endif

static int currentTemp = MAX_TEMPERATURE;
static int radiationProhibition = 0;

static void initHW(void);
static uint8_t slotNumber(void);
static void powerOnExciter(void);
static void enableRadiation(int enable);
static int supplyState(void);
static int afuState(void);
static int irpState(void);
static CanMessage buildReplyCanMessage(uint8_t sender, CanMessage *mess, uint8_t length);
static void updateTemperature(void);
static void initIndicatorGpioExpander(void);
static void sendLiteraNumberToIndication(uint8_t slot);
static void indicateAFU(int state);
static void indicateIRP(int state);
static void writeMcp23s17Register(uint8_t reg, uint16_t data);
static uint16_t literaToGpio(uint8_t slot);

//------------------------------------------------------------------------------

void initDevice(void)
{
    initHW();

    uint8_t slot = slotNumber();

    if (slot) {
        canAddress |= slot;
        CanMessage mess;
        mess.dest = ADDR_BROADCAST;
        mess.length = 6;
        mess.number = 0x00;
        mess.command = CMD_CHANGE_PARAM;
        mess.data[0] = PARAM_MODULE_SLOT;
        mess.data[1] = canAddress & 0x0F;
        canSend((uint8_t *)&mess, mess.length, canAddress);
    } else {
        while (1)
            ; // TODO stop program execution
    }

    sendLiteraNumberToIndication(litera);
    enableRadiation(0);
    powerOnExciter();
}

void process10ms(void)
{
    static int counter = 0;

    if (!counter) {
        int supply = supplyState();
        int afu = afuState();
        int irp = irpState();
        updateTemperature();
        if (!supply || !afu || radiationProhibition || currentTemp >= MAX_TEMPERATURE) {
            enableRadiation(0);
        }
        indicateAFU(afu);
        indicateIRP(irp);
    }

    if (++counter == 10)
        counter = 0;
}

void onCanMessageReceived(uint8_t sender, CanMessage *mess)
{
#if defined(DISCOVERY_MODE)
    flashLed(LED_4, 200, 0);
#endif

    if ((mess->dest & 0xF0) == (canAddress & 0xF0) || mess->dest == ADDR_BROADCAST) {
        switch (mess->command) {
        case CMD_ASK_SIGNAL:
            if (mess->data[0] == SIGNAL_AFU_STATE) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_SIGNAL;
                r.data[0] = SIGNAL_AFU_STATE;
                r.data[1] = afuState();
                canSend((uint8_t *)&r, r.length, canAddress);
            } else if (mess->data[0] == SIGNAL_IRP_STATE) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_SIGNAL;
                r.data[0] = SIGNAL_IRP_STATE;
                r.data[1] = irpState();
                canSend((uint8_t *)&r, r.length, canAddress);
            }
            break;
        case CMD_ASK_PARAM:
            if (mess->data[0] == PARAM_MODULE_SLOT) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_PARAM;
                r.data[0] = PARAM_MODULE_SLOT;
                r.data[1] = canAddress & 0x0F;
                canSend((uint8_t *)&r, r.length, canAddress);
            } else if (mess->data[0] == PARAM_RAD_PROHIBITION) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_PARAM;
                r.data[0] = PARAM_RAD_PROHIBITION;
                r.data[1] = radiationProhibition;
                canSend((uint8_t *)&r, r.length, canAddress);
            } else if (mess->data[0] == PARAM_TEMPERATURE) {
                CanMessage r = buildReplyCanMessage(sender, mess, 6);
                r.command = CMD_ANS_PARAM;
                r.data[0] = PARAM_TEMPERATURE;
                r.data[1] = currentTemp;
                canSend((uint8_t *)&r, r.length, canAddress);
            }
            break;
        case CMD_SET_SIGNAL:
            if (mess->data[0] == SIGNAL_RAD_CTRL) {
                if ((mess->data[1] && !radiationProhibition) || !mess->data[1]) {
                    enableRadiation(mess->data[1]);
                }
            } else if (mess->data[0] == SIGNAL_AFU_STATE) {
                // FIXME for debug only
                indicateAFU(mess->data[1]);
            } else if (mess->data[0] == SIGNAL_IRP_STATE) {
                // FIXME for debug only
                indicateIRP(mess->data[1]);
            }
            break;
        case CMD_SET_PARAM:
            if (mess->data[0] == PARAM_RAD_PROHIBITION) {
                radiationProhibition = mess->data[1];
            } else if (mess->data[0] == PARAM_MODULE_LITERA) {
                // FIXME for debug only
                sendLiteraNumberToIndication(mess->data[1]);
            }
            break;
        }
    }

    intFree(mess);
}

//------------------------------------------------------------------------------

static void initHW(void)
{
    initCanHW();
    initI2CHW();
    initSpiHW();

    // init indication gpio expander
    initIndicatorGpioExpander();

    GPIO_InitTypeDef gpio;

    // exciter
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_9;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio);

    // radiation control
    gpio.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio);

    // AFU state, IRP state
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_OType = GPIO_OType_OD;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN; // FIXME default state
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio);

    // AFU indication, IRP indication
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_9;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio);

    // exciter supply
    gpio.GPIO_Pin = GPIO_Pin_3;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_OType = GPIO_OType_OD;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN; // FIXME default state
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio);
}

static uint8_t slotNumber(void)
{
    GPIO_InitTypeDef gpio;
    uint8_t slot = 0;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_8 | GPIO_Pin_10;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_OType = GPIO_OType_OD;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio);

    if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9))
        slot |= 1;
    if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_8))
        slot |= 2;
    if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_10))
        slot |= 4;

    return slot;
}

//------------------------------------------------------------------------------
// exciter

static void powerOnExciter(void)
{
    GPIO_SetBits(GPIOC, GPIO_Pin_6);
    GPIO_SetBits(GPIOC, GPIO_Pin_9);
}

static void enableRadiation2(void)
{
    GPIO_SetBits(GPIOC, GPIO_Pin_7);
}

static void enableRadiation(int enable)
{
    if (!enable) {
        GPIO_ResetBits(GPIOC, GPIO_Pin_8);
        GPIO_ResetBits(GPIOC, GPIO_Pin_7);
    } else {
        if (!GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_8)) {
            GPIO_SetBits(GPIOC, GPIO_Pin_8);
            setTimeout(300, (pulf3ul)enableRadiation2, 0, 0, 0);
        }
    }
}

static int supplyState(void)
{
    return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3);
}

static int afuState(void)
{
    return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0);
}

static int irpState(void)
{
    return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);
}

static CanMessage buildReplyCanMessage(uint8_t sender, CanMessage *mess, uint8_t length)
{
    CanMessage reply;
    reply.dest = sender;
    reply.length = length;
    reply.number = mess->number ? (mess->number + 8) : 0x00;
    return reply;
}

//------------------------------------------------------------------------------
// termo sensor

static void updateTemperature(void)
{
#if !defined(DISCOVERY_MODE)
    int count = 0;

    while (I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)) ;
    I2C_GenerateSTART(I2C1, ENABLE);
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT)) ;
    I2C_Send7bitAddress(I2C1, TEMP_SENSOR_ADDR, I2C_Direction_Transmitter);
    for (count = 0; !I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) && count < I2C_TIMEOUT; count++) ;
    if (count >= I2C_TIMEOUT) {
        currentTemp = MAX_TEMPERATURE;
        return;
    }
    I2C_SendData(I2C1, SELECT_TEMP_REG);
    for (count = 0; !I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) && count < I2C_TIMEOUT; count++) ;
    if (count >= I2C_TIMEOUT) {
        currentTemp = MAX_TEMPERATURE;
        return;
    }
    I2C_GenerateSTART(I2C1, ENABLE);
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT)) ;
    I2C_Send7bitAddress(I2C1, TEMP_SENSOR_ADDR, I2C_Direction_Receiver);
    for (count = 0; !I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) && count < I2C_TIMEOUT; count++) ;
    if (count >= I2C_TIMEOUT) {
        currentTemp = MAX_TEMPERATURE;
        return;
    }
    for (count = 0; count < I2C_TIMEOUT; count++) {
        I2C_AcknowledgeConfig(I2C1, DISABLE);
        I2C_GenerateSTOP(I2C1, ENABLE);
        if (I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED)) {
            int value = I2C_ReceiveData(I2C1);
            if (value & 0x80)
                value = -1 * ((value ^ 0xFF) + 1);
            currentTemp = value;
            break;
        }
    }
    I2C_AcknowledgeConfig(I2C1, ENABLE);
#endif
}

//------------------------------------------------------------------------------
// indication module

static void initIndicatorGpioExpander(void)
{
    writeMcp23s17Register(MCP23S17_REG_IODIR, MCP23S17_DIR_OUT);
}

static void sendLiteraNumberToIndication(uint8_t litera)
{
    uint16_t data = literaToGpio(litera);
    writeMcp23s17Register(MCP23S17_REG_GPIO, data);
}

static void indicateAFU(int state)
{
    if (state)
        GPIO_ResetBits(GPIOA, GPIO_Pin_10);
    else
        GPIO_SetBits(GPIOA, GPIO_Pin_10);
}

static void indicateIRP(int state)
{
    if (state)
        GPIO_ResetBits(GPIOA, GPIO_Pin_9);
    else
        GPIO_SetBits(GPIOA, GPIO_Pin_9);
}

static void writeMcp23s17Register(uint8_t reg, uint16_t data)
{
    uint8_t buff[] = {MCP23S17_WRITE_CMD, reg, data, data >> 8};

    GPIO_ResetBits(INDICATE_SPI_GPIO, INDICATE_SPI_CS_PIN);

    for (int i = 0; i < sizeof(buff); i++) {
        spiSendByte(SPI2, buff[i]);
    }

    GPIO_SetBits(INDICATE_SPI_GPIO, INDICATE_SPI_CS_PIN);
}

static uint16_t literaToGpio(uint8_t litera)
{
    uint16_t data = 0x0000;

    switch (litera) {
    case 1:
        data = 0x0600;
        break;
    case 2:
        data = 0x5B00;
        break;
    case 3:
        data = 0x4F00;
        break;
    case 4:
        data = 0x6600;
        break;
    case 5:
        data = 0x6D00;
        break;
    }

    return data;
}

//------------------------------------------------------------------------------
