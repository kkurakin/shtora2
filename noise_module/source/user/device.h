//
//

#ifndef __DEVICE_H
#define __DEVICE_H

#define MAX_TEMPERATURE 60

#define TEMP_SENSOR_ADDR 0x4D
#define SELECT_TEMP_REG 0x00

#define I2C_TIMEOUT 1000

#define MCP23S17_WRITE_CMD 0x40
#define MCP23S17_REG_IODIR 0x00
#define MCP23S17_REG_GPIO 0x12
#define MCP23S17_DIR_OUT 0x0000

void initDevice(void);
void process10ms(void);

#endif
