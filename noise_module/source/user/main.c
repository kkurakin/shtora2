//
//

#include "clock.h"
#include "queue.h"
#include "device.h"
#if defined(DISCOVERY_MODE)
#include "discovery.h"
#endif

//------------------------------------------------------------------------------

static void init(void);
static void initHardware(void);

//------------------------------------------------------------------------------

int main(void)
{
    init();

    while (1) {
        if (sysFlag.f1ms) {
            sysFlag.f1ms = 0;
            checkTimeout();
        }
        if (sysFlag.f10ms) {
            sysFlag.f10ms = 0;
            process10ms();
        }
        runQueue();
    }
}

//------------------------------------------------------------------------------

static void init(void)
{
    initSysTicks();

    initHardware();

    initDevice();
}

static void initHardware(void)
{
#if defined(DISCOVERY_MODE)
    initLeds();
#endif
}

//------------------------------------------------------------------------------
