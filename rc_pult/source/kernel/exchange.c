//
//

//#include <stdlib.h>
#include <string.h>
#include "exchange.h"
#include "queue.h"
#include "message.h"
#include "memory.h"
#include "can.h"

//------------------------------------------------------------------------------

static ExecCheck *pTabCheck[MAX_MESSAGE_COUNT] = {0};

//------------------------------------------------------------------------------

static void startTr(ExecCheck *p);

//------------------------------------------------------------------------------

void checkExchange(void)
{
    for (int i = 0; i < MAX_MESSAGE_COUNT; i++) {
        ExecCheck *p = pTabCheck[i];
        if (!p)
            continue;
        if (!p->elapsedTime || --p->elapsedTime)
            continue;
        if (!p->attempt) {
            if (p->errHandler)
                pushQueue(p->errHandler, (uint32_t)p->mess, i + 1, 0);
            else
                freeCheckPoint(i + 1);
        } else {
            p->attempt--;
            p->elapsedTime = p->waitingPeriod;
            startTr(p);
        }
    }
}

int checkExchangeMessage(void *mess)
{
    CanMessage *m = (CanMessage *)mess;
    uint32_t number = m->number;
    if (number > 8) {
        pulf3ul handler = getAnswHandler(number);
        if (handler) {
            pushQueue(handler, (uint32_t)mess, number & 0x07, 0);
            return 1;
        }
    }

    return 0;
}

uint32_t getNewNumber(void *mess, pulf3ul answ, pulf3ul err)
{
    CanMessage *m = (CanMessage *)mess;
    for (int i = 0; i < MAX_MESSAGE_COUNT; i++) {
        if (!pTabCheck[i]) {
            ExecCheck *p = intMalloc(sizeof(ExecCheck));
            if (!p)
                return (uint32_t)-1;
            p->waitingPeriod = DEFAULT_WAIT_PERIOD;
            p->attempt = DEFAULT_ATTEMPT;
            p->answHandler = answ;
            p->errHandler = err;
            p->mess = mess;
            pTabCheck[i] = p;
            m->number = i + 1;
            return i + 1;
        }
    }
    return 0;
}

void alterCheck(uint32_t number, uint16_t waitingPeriod, uint32_t attempt)
{
    if (!number || number > MAX_MESSAGE_COUNT)
        return;

    ExecCheck *p = pTabCheck[--number];
    if (!p)
        return;

    if (waitingPeriod)
        p->waitingPeriod = waitingPeriod;
    if (attempt)
        p->attempt = attempt;
}

void trCommand(uint32_t number)
{
    if (!number || number > MAX_MESSAGE_COUNT)
        return;

    ExecCheck *p = pTabCheck[--number];
    if (!p)
        return;

    p->attempt--;
    p->elapsedTime = p->waitingPeriod;

    startTr(p);
}

pulf3ul getAnswHandler(uint32_t number)
{
    number -= 9; // TODO define value with #define

    if (number >= MAX_MESSAGE_COUNT)
        return 0;

    if (!pTabCheck[number])
        return 0;

    return pTabCheck[number]->answHandler;
}

void freeCheckPoint(uint32_t number)
{
    if (!number || number > MAX_MESSAGE_COUNT)
        return;

    ExecCheck *p = pTabCheck[--number];
    if (!p)
        return;

    intFree(p->mess);
    intFree(p);

    pTabCheck[number] = 0;
}

void freeOnlyCheckPoint(uint32_t number)
{
    if (!number || number > MAX_MESSAGE_COUNT)
        return;

    ExecCheck *p = pTabCheck[--number];
    if (!p)
        return;

    intFree(p);

    pTabCheck[number] = 0;
}

//------------------------------------------------------------------------------

static void startTr(ExecCheck *p)
{
    CanMessage *m = (CanMessage *)p->mess;
    pushQueue((pulf3ul)canSend, (uint32_t)m, m->length, ADDR_RC_PULT);
}

//------------------------------------------------------------------------------
