//
//

#ifndef __EXCHANGE_H
#define __EXCHANGE_H

#include <stdint.h>
#include "queue.h"

#define DEFAULT_WAIT_PERIOD 1000
#define DEFAULT_ATTEMPT 1

#define MAX_MESSAGE_COUNT 7

typedef struct {
    pulf3ul answHandler;
    pulf3ul errHandler;
    void *mess;
    uint16_t elapsedTime;
    uint16_t waitingPeriod;
    uint8_t attempt;
} ExecCheck;

void checkExchange(void);
uint32_t getNewNumber(void *mess, pulf3ul answ, pulf3ul err);
void alterCheck(uint32_t number, uint16_t waitingPeriod, uint32_t attempt);
void trCommand(uint32_t number);
pulf3ul getAnswHandler(uint32_t number);
void freeCheckPoint(uint32_t number);
void freeOnlyCheckPoint(uint32_t number);

#endif
