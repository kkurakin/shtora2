//
//

#include <intrinsics.h>
#include "queue.h"

//------------------------------------------------------------------------------

#define EXEC_QUEUE_SIZE 128
#define WAIT_QUEUE_SIZE 32

static RecQ execQueue[EXEC_QUEUE_SIZE];
static RecW waitQueue[WAIT_QUEUE_SIZE];
static volatile uint32_t wr_queue, rd_queue;

//------------------------------------------------------------------------------

int pushQueue(pulf3ul func, uint32_t param1, uint32_t param2, uint32_t param3)
{
    int result = 0;

    __istate_t state = __get_interrupt_state();
    __disable_interrupt();

    uint32_t diff = rd_queue - wr_queue;

    if (diff > EXEC_QUEUE_SIZE)
        diff += EXEC_QUEUE_SIZE;
    if (func && diff != 1) {
        execQueue[wr_queue].func = func;
        execQueue[wr_queue].param[0] = param1;
        execQueue[wr_queue].param[1] = param2;
        execQueue[wr_queue].param[2] = param3;
        if (++wr_queue >= EXEC_QUEUE_SIZE)
            wr_queue = 0;
        result = 1;
    }

    __set_interrupt_state(state);

    return result;
}

void runQueue(void)
{
    while (rd_queue != wr_queue) {
        RecQ *p = &execQueue[rd_queue];
        p->func(p->param[0], p->param[1], p->param[2]);
        if (++rd_queue >= EXEC_QUEUE_SIZE)
            rd_queue = 0;
    }
}

void checkTimeout(void)
{
    for (int i = 0; i < WAIT_QUEUE_SIZE; i++) {
        if (waitQueue[i].time && !--waitQueue[i].time)
            pushQueue(waitQueue[i].rec.func, waitQueue[i].rec.param[0], waitQueue[i].rec.param[1], waitQueue[i].rec.param[2]);
    }
}

void setTimeout(uint32_t time, pulf3ul func, uint32_t param1, uint32_t param2, uint32_t param3)
{
    for (int i = 0; i < WAIT_QUEUE_SIZE; i++) {
        if (!waitQueue[i].time) {
            waitQueue[i].time = time;
            waitQueue[i].rec.func = func;
            waitQueue[i].rec.param[0] = param1;
            waitQueue[i].rec.param[1] = param2;
            waitQueue[i].rec.param[2] = param3;
            return;
        }
    }
}

void clrTimeout(pulf3ul func)
{
    for (int i = 0; i < WAIT_QUEUE_SIZE; i++) {
        if (waitQueue[i].time && waitQueue[i].rec.func == func)
            waitQueue[i].time = 0;
    }
}

//------------------------------------------------------------------------------
