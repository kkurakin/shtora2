//
//

#include <stdlib.h>
#include <string.h>
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "device.h"
#include "queue.h"
#include "can.h"
#include "message.h"
#include "memory.h"
#include "power.h"
#if defined(DISCOVERY_MODE)
#include "discovery.h"
#endif

//------------------------------------------------------------------------------

static const uint8_t noiseModules[5] = {
    ADDR_LITERA_1,
    ADDR_LITERA_2,
    ADDR_LITERA_3,
    ADDR_LITERA_4,
    ADDR_LITERA_5
};

static const struct {
    GPIO_TypeDef *gpio;
    uint32_t pin;
} akbLeds[] = {
    {AKB_LOW_LED_GPIO, AKB_LOW_LED_GPIO_PIN},
    {AKB_25_LED_GPIO, AKB_25_LED_GPIO_PIN},
    {AKB_50_LED_GPIO, AKB_50_LED_GPIO_PIN},
    {AKB_75_LED_GPIO, AKB_75_LED_GPIO_PIN},
    {AKB_100_LED_GPIO, AKB_100_LED_GPIO_PIN}
};

static const struct {
    GPIO_TypeDef *gpio;
    uint32_t pin;
} greenLeds[] = {
    {RAD_GREEN_LED_1_GPIO, RAD_GREEN_LED_1_GPIO_PIN},
    {RAD_GREEN_LED_2_GPIO, RAD_GREEN_LED_2_GPIO_PIN},
    {RAD_GREEN_LED_3_GPIO, RAD_GREEN_LED_3_GPIO_PIN},
    {RAD_GREEN_LED_4_GPIO, RAD_GREEN_LED_4_GPIO_PIN},
    {RAD_GREEN_LED_5_GPIO, RAD_GREEN_LED_5_GPIO_PIN},
    {RAD_GREEN_LED_COMMON_GPIO, RAD_GREEN_LED_COMMON_GPIO_PIN}
};

static const struct {
    GPIO_TypeDef *gpio;
    uint32_t pin;
} redLeds[] = {
    {RAD_RED_LED_1_GPIO, RAD_RED_LED_1_GPIO_PIN},
    {RAD_RED_LED_2_GPIO, RAD_RED_LED_2_GPIO_PIN},
    {RAD_RED_LED_3_GPIO, RAD_RED_LED_3_GPIO_PIN},
    {RAD_RED_LED_4_GPIO, RAD_RED_LED_4_GPIO_PIN},
    {RAD_RED_LED_5_GPIO, RAD_RED_LED_5_GPIO_PIN},
    {RAD_RED_LED_COMMON_GPIO, RAD_RED_LED_COMMON_GPIO_PIN}
};

static uint8_t canAddress = ADDR_RC_PULT;
static uint8_t akbMode = 0;
static uint8_t akbCharge = 0;
static uint8_t radiation = 0;
static uint8_t available[5] = {0, 0, 0, 0, 0};
static uint8_t afu[5] = {1, 1, 1, 1, 1};
static uint8_t irp[5] = {0, 0, 0, 0, 0};

static void initHW(void);
static void turnDevicePower(uint8_t state);
static void requestPowerMode(void);
static void requestAkbCharge(void);
static void requestAfu(void);
static void requestIrp(void);
static void checkPowerButton(void);
static void checkRadiationButton(void);
static void indicateOperability(uint8_t state);
static void indicateAkbCharge(uint8_t charge);
static void indicateAfuIrp(void);
#if defined(DISCOVERY_MODE)
static void fireLeds(int stage);
#endif

//------------------------------------------------------------------------------

void initPowerButton(void)
{
    GPIO_InitTypeDef gpio;

    RCC_AHB1PeriphClockCmd(PWR_BTN_CLOCK, ENABLE);

    gpio.GPIO_Pin = PWR_BTN_GPIO_PIN;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(PWR_BTN_GPIO, &gpio);
}

void initDevice(void)
{
    initHW();
    initPowerButton();

    turnDevicePower(1);
    indicateOperability(1);
    indicateAkbCharge(0);

#ifdef DISCOVERY_MODE
    fireLeds(0);
#endif
}

void process10ms(void)
{
    static uint8_t counter = 0;

    if (!counter) {
        requestPowerMode();
        requestAkbCharge();
        requestAfu();
        requestIrp();
        indicateAfuIrp();
        checkRadiationButton();
        checkPowerButton();
    }

    if (++counter == 10)
        counter = 0;
}

uint8_t isPowerButtonPressed(void)
{
    return GPIO_ReadInputDataBit(PWR_BTN_GPIO, PWR_BTN_GPIO_PIN);
}

void onCanMessageReceived(uint8_t sender, CanMessage *mess)
{
    if (mess->dest == canAddress || mess->dest == ADDR_BROADCAST) {
        switch (mess->command) {
        case CMD_ANS_SIGNAL:
            if (mess->data[0] == SIGNAL_AFU_STATE) {
                for (int i = 0; i < sizeof(noiseModules); i++) {
                    if ((sender & 0xF0) == noiseModules[i]) {
                        afu[i] = mess->data[1];
                        available[i] = 1;
                    }
                }
            } else if (mess->data[0] == SIGNAL_IRP_STATE) {
                for (int i = 0; i < sizeof(noiseModules); i++) {
                    if ((sender & 0xF0) == noiseModules[i])
                        irp[i] = mess->data[1];
                }
            }
            break;
        case CMD_ANS_PARAM:
            if (mess->data[0] == PARAM_AKB_MODE) {
                akbMode = mess->data[1];
            } else if (mess->data[0] == PARAM_AKB_CHARGE) {
                akbCharge = mess->data[1];
            }
            break;
        }
    }

    free(mess);
}

//------------------------------------------------------------------------------

static void initHW(void)
{
    initCanHW();

    GPIO_InitTypeDef gpio;

    // power button
    // already configured in power module

    // radiation button
    RCC_AHB1PeriphClockCmd(RAD_BTN_CLOCK, ENABLE);
    gpio.GPIO_Pin = RAD_BTN_PIN;
    gpio.GPIO_Mode = GPIO_Mode_IN;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(RAD_BTN_GPIO, &gpio);

    // green radiation leds
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); // TODO gpio clock to struct
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    for (int i = 0; i < sizeof(greenLeds) / sizeof(greenLeds[0]); i++) {
        gpio.GPIO_Pin = greenLeds[i].pin;
        GPIO_Init(greenLeds[i].gpio, &gpio);
    }

    // red radiation leds
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); // TODO gpio clock to struct
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    for (int i = 0; i < sizeof(redLeds) / sizeof(redLeds[0]); i++) {
        gpio.GPIO_Pin = redLeds[i].pin;
        GPIO_Init(redLeds[i].gpio, &gpio);
    }

    // AKB leds
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); // TODO gpio clock to struct
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    for (int i = 0; i < sizeof(akbLeds) / sizeof(akbLeds[0]); i++) {
        gpio.GPIO_Pin = akbLeds[i].pin;
        GPIO_Init(akbLeds[i].gpio, &gpio);
    }

    // operability led
    RCC_AHB1PeriphClockCmd(OPERABILITY_LED_CLOCK, ENABLE);
    gpio.GPIO_Pin = OPERABILITY_LED_PIN;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(OPERABILITY_LED_GPIO, &gpio);

    // power control gpio
    RCC_AHB1PeriphClockCmd(DEV_PWR_CTRL_CLOCK, ENABLE);
    gpio.GPIO_Pin = DEV_PWR_CTRL_PIN;
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(DEV_PWR_CTRL_GPIO, &gpio);
}

static void turnDevicePower(uint8_t state)
{
    if (state)
        GPIO_SetBits(DEV_PWR_CTRL_GPIO, DEV_PWR_CTRL_PIN);
    else
        GPIO_ResetBits(DEV_PWR_CTRL_GPIO, DEV_PWR_CTRL_PIN);
}

static void requestPowerMode(void)
{
    CanMessage mess;

    mess.dest = ADDR_CTRL_MODULE;
    mess.length = 5;
    mess.number = 0x00;
    mess.command = CMD_ASK_PARAM;
    mess.data[0] = PARAM_AKB_MODE;

    canSend((uint8_t *)&mess, mess.length, canAddress);
}

static void requestAkbCharge(void)
{
    CanMessage mess;

    mess.dest = ADDR_CTRL_MODULE;
    mess.length = 5;
    mess.number = 0x00;
    mess.command = CMD_ASK_PARAM;
    mess.data[0] = PARAM_AKB_CHARGE;

    canSend((uint8_t *)&mess, mess.length, canAddress);
}

static void requestAfu(void)
{
    CanMessage mess;

    mess.length = 5;
    mess.number = 0x00;
    mess.command = CMD_ASK_SIGNAL;
    mess.data[0] = SIGNAL_AFU_STATE;

    for (int i = 0; i < sizeof(noiseModules); i++) {
        mess.dest = noiseModules[i];
        canSend((uint8_t *)&mess, mess.length, canAddress);
    }
}

static void requestIrp(void)
{
    CanMessage mess;

    mess.length = 5;
    mess.number = 0x00;
    mess.command = CMD_ASK_SIGNAL;
    mess.data[0] = SIGNAL_IRP_STATE;

    for (int i = 0; i < sizeof(noiseModules); i++) {
        mess.dest = noiseModules[i];
        canSend((uint8_t *)&mess, mess.length, canAddress);
    }
}

static void checkPowerButton(void)
{
    static int pressedCount = 0;

    if (!GPIO_ReadInputDataBit(PWR_BTN_GPIO, PWR_BTN_GPIO_PIN)) {
        pressedCount = 0;
    } else if (++pressedCount >= 10) {
        // pressed during 1 second
        turnDevicePower(0);
        indicateOperability(0);
        standby();
    }
}

static void checkRadiationButton(void)
{
    static int pressedCount = 0;

    if (!GPIO_ReadInputDataBit(RAD_BTN_GPIO, RAD_BTN_PIN)) {
        pressedCount = 0;
    } else if (++pressedCount >= 10) {
        // pressed during 1 second
        radiation ^= 1;
        CanMessage mess;
        mess.dest = ADDR_BROADCAST;
        mess.length = 6;
        mess.number = 0x00;
        mess.command = CMD_SET_SIGNAL;
        mess.data[0] = SIGNAL_RAD_CTRL;
        mess.data[1] = radiation;
        canSend((uint8_t *)&mess, mess.length, canAddress);
    }
}

static void indicateOperability(uint8_t state)
{
    if (state)
        GPIO_SetBits(OPERABILITY_LED_GPIO, OPERABILITY_LED_PIN);
    else
        GPIO_ResetBits(OPERABILITY_LED_GPIO, OPERABILITY_LED_PIN);
}

static void indicateAkbCharge(uint8_t stage)
{
    void (*func)(GPIO_TypeDef *, uint16_t);
    if (akbMode)
        func = stage ? GPIO_SetBits : GPIO_ResetBits;
    else
        func = GPIO_SetBits;

    switch (akbCharge) {
    case AKB_CHARGE_100_:
        func(AKB_100_LED_GPIO, AKB_100_LED_GPIO_PIN);
    case AKB_CHARGE_75_:
        func(AKB_75_LED_GPIO, AKB_75_LED_GPIO_PIN);
    case AKB_CHARGE_50_:
        func(AKB_50_LED_GPIO, AKB_50_LED_GPIO_PIN);
    case AKB_CHARGE_25_:
        func(AKB_25_LED_GPIO, AKB_25_LED_GPIO_PIN);
    case AKB_CHARGE_LOW:
        func(AKB_LOW_LED_GPIO, AKB_LOW_LED_GPIO_PIN);
        break;
    }

    switch (akbCharge) {
    case AKB_CHARGE_LOW:
        GPIO_ResetBits(AKB_25_LED_GPIO, AKB_25_LED_GPIO_PIN);
    case AKB_CHARGE_25_:
        GPIO_ResetBits(AKB_50_LED_GPIO, AKB_50_LED_GPIO_PIN);
    case AKB_CHARGE_50_:
        GPIO_ResetBits(AKB_75_LED_GPIO, AKB_75_LED_GPIO_PIN);
    case AKB_CHARGE_75_:
        GPIO_ResetBits(AKB_100_LED_GPIO, AKB_100_LED_GPIO_PIN);
        break;
    }

    setTimeout(250, (pulf3ul)indicateAkbCharge, !stage, 0, 0);
}

static void indicateAfuIrp(void)
{
    uint8_t commonAfu = 1;
    uint8_t commonIrp = 1;
    uint8_t commonIndex = sizeof(greenLeds) / sizeof(greenLeds[0]) - 1;

    for (int i = 0; i < sizeof(afu); i++) {
        if (!afu[i]) {
            GPIO_SetBits(redLeds[i].gpio, redLeds[i].pin);
            GPIO_ResetBits(greenLeds[i].gpio, greenLeds[i].pin);
        } else if (!irp[i]) {
            GPIO_ResetBits(redLeds[i].gpio, redLeds[i].pin);
            GPIO_ResetBits(greenLeds[i].gpio, greenLeds[i].pin);
        } else {
            GPIO_ResetBits(redLeds[i].gpio, redLeds[i].pin);
            GPIO_SetBits(greenLeds[i].gpio, greenLeds[i].pin);
        }
        if (available[i]) {
            if (!afu[i])
                commonAfu = 0;
            if (!irp[i])
                commonIrp = 0;
        }
    }

    if (!commonAfu) {
        GPIO_SetBits(redLeds[commonIndex].gpio, redLeds[commonIndex].pin);
        GPIO_ResetBits(greenLeds[commonIndex].gpio, greenLeds[commonIndex].pin);
    } else if (!commonIrp) {
        GPIO_ResetBits(redLeds[commonIndex].gpio, redLeds[commonIndex].pin);
        GPIO_ResetBits(greenLeds[commonIndex].gpio, greenLeds[commonIndex].pin);
    } else {
        GPIO_ResetBits(redLeds[commonIndex].gpio, redLeds[commonIndex].pin);
        GPIO_SetBits(greenLeds[commonIndex].gpio, greenLeds[commonIndex].pin);
    }
}

//------------------------------------------------------------------------------

#ifdef DISCOVERY_MODE

static void fireLeds(int stage)
{
    switch (stage) {
    case 0:
        setLed(LED_4, 0);
        setLed(LED_1, 1);
        stage = 1;
        break;
    case 1:
        setLed(LED_1, 0);
        setLed(LED_2, 1);
        stage = 2;
        break;
    case 2:
        setLed(LED_2, 0);
        setLed(LED_3, 1);
        stage = 3;
        break;
    case 3:
        setLed(LED_3, 0);
        setLed(LED_4, 1);
        stage = 0;
        break;
    }

    setTimeout(500, (pulf3ul)fireLeds, stage, 0, 0);
}

#endif

//------------------------------------------------------------------------------
