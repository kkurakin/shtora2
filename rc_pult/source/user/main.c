//
//

#include "stm32f4xx.h"
#include "clock.h"
#include "queue.h"
#include "exchange.h"
#include "device.h"
#include "power.h"
#if defined(DISCOVERY_MODE)
#include "discovery.h"
#endif

//------------------------------------------------------------------------------

static void init(void);
static void initHardware(void);

//------------------------------------------------------------------------------

int main(void)
{
    // enable debug in standby mode (test needed)
    *((volatile uint32_t *)0xE0042004) |= DBGMCU_CR_DBG_STANDBY;

    initRtc();
    initPowerButton();
    checkWakeup();

    init();

    while (1) {
        if (sysFlag.f1ms) {
            sysFlag.f1ms = 0;
            checkTimeout();
            checkExchange();
        }
        if (sysFlag.f10ms) {
            sysFlag.f10ms = 0;
            process10ms();
        }
        runQueue();
    }
}

//------------------------------------------------------------------------------

static void init(void)
{
    initSysTicks();

    initHardware();

    initDevice();
}

static void initHardware(void)
{
#if defined(DISCOVERY_MODE)
    initLeds();
#endif
}

//------------------------------------------------------------------------------
