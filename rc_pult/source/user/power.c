//
//

#include "power.h"
#include "device.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_pwr.h"
#include "stm32f4xx_rtc.h"
#include "misc.h"

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void initRtc(void)
{
    EXTI_InitTypeDef exti;
    NVIC_InitTypeDef nvic;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    PWR_BackupAccessCmd(ENABLE);
    PWR_ClearFlag(PWR_FLAG_WU);
    RTC_ClearITPendingBit(RTC_IT_WUT);
    if (PWR_GetFlagStatus(PWR_FLAG_SB) != RESET) {
        PWR_ClearFlag(PWR_FLAG_SB);
        RTC_WaitForSynchro();
    } else {
        RCC_BackupResetCmd(ENABLE);
        RCC_BackupResetCmd(DISABLE);
        RCC_LSICmd(ENABLE);
        while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET) ;
        RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
        RCC_RTCCLKCmd(ENABLE);
        RTC_WaitForSynchro();
        EXTI_ClearITPendingBit(EXTI_Line22);
        exti.EXTI_Line = EXTI_Line22;
        exti.EXTI_Mode = EXTI_Mode_Interrupt;
        exti.EXTI_Trigger = EXTI_Trigger_Rising;
        exti.EXTI_LineCmd = ENABLE;
        EXTI_Init(&exti);
        nvic.NVIC_IRQChannel = RTC_WKUP_IRQn;
        nvic.NVIC_IRQChannelPreemptionPriority = 0;
        nvic.NVIC_IRQChannelSubPriority = 0;
        nvic.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvic);
        RTC_ITConfig(RTC_IT_WUT, ENABLE);
        RTC_WriteBackupRegister(BTN_PRESS_COUNT_REG, 0);
    }
}

void checkWakeup(void)
{
    if (isPowerButtonPressed()) {
        uint32_t pressCount = RTC_ReadBackupRegister(BTN_PRESS_COUNT_REG);
        RTC_WriteBackupRegister(BTN_PRESS_COUNT_REG, ++pressCount);
        if (pressCount >= 5) { // TODO define count value
            RTC_ITConfig(RTC_IT_WUT, DISABLE);
            RTC_WakeUpCmd(DISABLE);
            return;
        }
    } else {
        RTC_WriteBackupRegister(BTN_PRESS_COUNT_REG, 0);
    }

    // button not pressed, sleep
    RTC_WakeUpCmd(DISABLE);
    RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
    RTC_SetWakeUpCounter(0x1FF); // 250 ms
    RTC_WakeUpCmd(ENABLE);
    PWR_EnterSTANDBYMode();
}

void standby(void)
{
    RTC_WriteBackupRegister(BTN_PRESS_COUNT_REG, 0);
    RTC_ITConfig(RTC_IT_WUT, ENABLE);
    RTC_SetWakeUpCounter(0x1FF); // 250 ms
    RTC_WakeUpCmd(ENABLE);
    PWR_EnterSTANDBYMode();
}

//------------------------------------------------------------------------------

void RTC_WKUP_IRQHandler(void)
{
    if (RTC_GetITStatus(RTC_IT_WUT) != RESET) {
        RTC_ClearITPendingBit(RTC_IT_WUT);
        EXTI_ClearITPendingBit(EXTI_Line22);
    }
}

//------------------------------------------------------------------------------
