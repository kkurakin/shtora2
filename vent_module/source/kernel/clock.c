//
//

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "clock.h"

//------------------------------------------------------------------------------

static volatile uint32_t sysTicks = 0;

SysFlag sysFlag;

//------------------------------------------------------------------------------

void initSysTicks(void)
{
    RCC_ClocksTypeDef RCC_Clocks;

    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
}

void SysTick_Handler(void)
{
    sysTicks++;

    sysFlag.f1ms = 1;

    if (sysTicks - sysFlag.ticks10ms >= 10) {
        sysFlag.ticks10ms = sysTicks;
        sysFlag.f10ms = 1;
    }
}

//------------------------------------------------------------------------------
