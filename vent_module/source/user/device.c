//
//

#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_tim.h"
#include "device.h"
#include "queue.h"

//------------------------------------------------------------------------------

#define NUMBER_OF_STEPS 20
#define STEP_TIME (4000 / NUMBER_OF_STEPS)

static uint16_t pulseMin;
static uint16_t pulseMax;
static TIM_OCInitTypeDef oc;

static void updatePWM(uint32_t step);

//------------------------------------------------------------------------------

void initDevice(void)
{
    GPIO_InitTypeDef gpio;
    TIM_TimeBaseInitTypeDef timer;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    gpio.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_PuPd = GPIO_PuPd_UP ;
    GPIO_Init(GPIOA, &gpio);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_2);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    uint16_t period = (SystemCoreClock / 100) - 1;
    pulseMin = (uint16_t)(((uint32_t)7 * (period - 1)) / 10);
    pulseMax = (uint16_t)(((uint32_t)10 * (period - 1)) / 10);

    timer.TIM_Prescaler = 0;
    timer.TIM_CounterMode = TIM_CounterMode_Up;
    timer.TIM_Period = period;
    timer.TIM_ClockDivision = 0;
    timer.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, &timer);

    oc.TIM_OCMode = TIM_OCMode_PWM2;
    oc.TIM_OutputState = TIM_OutputState_Enable;
    oc.TIM_OutputNState = TIM_OutputNState_Enable;
    oc.TIM_Pulse = pulseMin;
    oc.TIM_OCPolarity = TIM_OCPolarity_Low;
    oc.TIM_OCNPolarity = TIM_OCNPolarity_High;
    oc.TIM_OCIdleState = TIM_OCIdleState_Set;
    oc.TIM_OCNIdleState = TIM_OCIdleState_Reset;
    TIM_OC3Init(TIM2, &oc);
    TIM_OC4Init(TIM2, &oc);

    TIM_Cmd(TIM2, ENABLE);

    TIM_CtrlPWMOutputs(TIM2, ENABLE);

    setTimeout(STEP_TIME, (pulf3ul)updatePWM, 1, 0, 0);
}

void process10ms(void)
{
}

//------------------------------------------------------------------------------

static void updatePWM(uint32_t step)
{
    oc.TIM_Pulse = pulseMin + step * (pulseMax - pulseMin) / NUMBER_OF_STEPS;
    TIM_OC3Init(TIM2, &oc);
    TIM_OC4Init(TIM2, &oc);

    if (step < NUMBER_OF_STEPS)
        setTimeout(STEP_TIME, (pulf3ul)updatePWM, ++step, 0, 0);
}

//------------------------------------------------------------------------------
