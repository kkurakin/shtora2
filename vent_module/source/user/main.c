//
//

#include "clock.h"
#include "queue.h"
#include "device.h"

//------------------------------------------------------------------------------

static void init(void)
{
    initSysTicks();

    initDevice();
}

void main(void)
{
    init();

    while (1) {
        if (sysFlag.f1ms) {
            sysFlag.f1ms = 0;
            checkTimeout();
        }
        if (sysFlag.f10ms) {
            sysFlag.f10ms = 0;
            process10ms();
        }
        runQueue();
    }
}

//------------------------------------------------------------------------------
